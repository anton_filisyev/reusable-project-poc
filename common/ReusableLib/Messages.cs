﻿namespace ReusableLib
{
    public static class Messages
    {
        public const string Hello = "Hello!";
        public const string HelloUserTemplate = "Hello {0}!";
    }
}

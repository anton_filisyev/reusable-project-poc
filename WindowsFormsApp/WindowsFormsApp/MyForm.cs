﻿using System.Windows.Forms;
using ReusableLib;

namespace WindowsFormsApp
{
    public partial class MyForm : Form
    {
        public MyForm()
        {
            InitializeComponent();

            lbHello.Text = Messages.Hello;
        }
    }
}
